﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetObject : MonoBehaviour
{
    public GameObject ballObject;

    [SerializeField]
    float forwardDistance = 1.0f;

    [SerializeField]
    float upwardDistance = 1.0f;

    public void ResetPosition()
    {
        //gameObject.transform.position = Camera.main.transform.position + (Camera.main.transform.up * upwardDistance) + (Camera.main.transform.forward * forwardDistance);

        //gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
        //gameObject.GetComponent<Rigidbody>().rotation = Quaternion.identity;

        ballObject.transform.position = new Vector3(Random.Range(-2.54f, 3.13f), Random.Range(0.92f, 2.62f), Random.Range(0.143f, 1.537f));
        ballObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
        ballObject.GetComponent<Rigidbody>().rotation = Quaternion.identity;
    }
}